# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 20:40:39 2020

@author: Kevin Nebiolo

Create a Banks Lake Model to test out Stryke
"""
# Import Dependencies
import stryke
import os
import numpy as np
import sqlite3 
import pandas as pd

# import excel sheet with scenarios
scenarios = pd.read_excel(r'C:\Users\Isha Deo\Documents\Projects\1823003_Stryke\Banks Lake Entrainment - Stryke Runs.xlsx',
                          sheet_name = 'Scenarios',index_col = 0)
summary_stats_df = pd.DataFrame()
for scenario, row in scenarios.iterrows():
    
    # set up workspaces
    proj_dir = r"C:\Users\Isha Deo\Documents\Projects\1823003_Stryke\Banks Lake Entrainment"
    dbName = "bankslake_scen%d.db"%int(scenario)
    dbDir = os.path.join(proj_dir,'Data',dbName)
    
    # create project
    banks_lake = stryke.create_proj_db(proj_dir,dbName)
    conn = sqlite3.connect(dbDir, timeout=30.0)
    c = conn.cursor()  
    
    # IPD: Kevin - insert excel reading here
    c.execute("INSERT INTO tblNodes VALUES ('tailrace','a priori',1)")
    c.execute("INSERT INTO tblNodes VALUES ('pump','Pump',0)")
    c.execute("INSERT INTO tblNodes VALUES ('turbine','Francis',0)")
    c.execute("INSERT INTO tblNodes VALUES ('forebay','a priori',1)")
    c.execute("INSERT INTO tblNodes VALUES ('tailrace_exit','a priori',1)")
    c.execute("INSERT INTO tblNodes VALUES ('forebay_exit','a priori',1)")
    
    
    c.execute("INSERT INTO tblEdges VALUES ('turbine','tailrace',0.5)")
    c.execute("INSERT INTO tblEdges VALUES ('forebay','turbine',%f)"%row['Entrainment Probability'])
    c.execute("INSERT INTO tblEdges VALUES ('tailrace','pump',%f)"%row['Entrainment Probability'])
    c.execute("INSERT INTO tblEdges VALUES ('pump','forebay',0.5)")
    c.execute("INSERT INTO tblEdges VALUES ('tailrace','tailrace_exit',%f)"%(1-row['Entrainment Probability']))
    c.execute("INSERT INTO tblEdges VALUES ('forebay','forebay_exit',%f)"%(1-row['Entrainment Probability']))
    
    
    c.execute("INSERT INTO tblFrancis VALUES ('turbine',%d,150,17.04,7513,7062.22,0,9,1.06,19,15.08,3.667,0.2)"%row['Head'])
    c.execute("INSERT INTO tblPump VALUES ('pump',%d,150,17.04,7513,7062.22,0,9,19,15.08,3.667,7062.22,0.2,0.153)"%row['Head'])
    
    conn.commit()
    c.close()
    
    # create routing network and append to project database
    route = stryke.create_route(dbDir)
    fish_type = row['Fish']
    fish_lenparams = ((row['Mean Length (mm)']/304.8),(row['Standard Deviation Length (mm)']/304.8))
    # iterate through 10 simulations
    for i in np.arange(0,10,1):
        # iterate over 100 fish
        for j in np.arange(0,100,1):
            # create a fish object, supply it with a species, log normal (mean, standard deviation) tuple, migration route, and database directory
            fish = stryke.fish(fish_type,fish_lenparams, route, dbDir,i,j)
            #print('fish number:%d, simulation:%d'%(j,i))
            # while fish is alive and it hasn't completed migrating through project 
            while fish.status == 1 and fish.complete == 0:
                # assess survival at this node and write to database
                fish.survive()
                # move to the next node- do we care enough about this data to log it?
                fish.move()
    # summarize
    stryke.summary(dbDir,fish_lenparams)
    #c = conn.cursor()  
    #c.execute("SELECT PercentSurvival FROM tblSummary")
    summary_stats_df[scenario] = [i[0] for i in pd.read_sql_query("SELECT PercentSurvival FROM tblSummary",conn).values]
    conn.close()
